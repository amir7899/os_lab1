#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
int main(int argc, char *argv[])
{
    char str1[15];
    char str2[15];
    for (int i = 0; i < strlen(argv[1]); i++)
    {
        str1[i] = argv[1][i];
    }
    str1[strlen(argv[1])] = 0;
    for (int i = 0; i < strlen(argv[2]); i++)
    {
        str2[i] = argv[2][i];
    }
    str2[strlen(argv[2])] = 0;
    char ans[15];
    int _file;
    int i = 0;
    int len1 = strlen(argv[1]);
    int len2 = strlen(argv[2]);
    while ( i < len1 && i <len2 )
    {
        if (str1[i] < str2[i])
        {
            ans[i] = '1';
        }
        else
        {
            ans[i] = '0';
        }
        i++;
    }
    if(len1 != len2)
    {
        if(len1 > len2)
        {
            int j = len1 - len2;
            while (j > 0)
            {
                ans[i] = '0';
                i++;
                j--;
            }
        }
        else
        {
            int j = len2 - len1;
            while (j > 0)
            {
                ans[i] = '1';
                i++;
                j--;
            }
        }
    }
    ans[i] = '\n';
    ans[i+1] = 0;
    _file = open("strdiff_result.txt",  O_CREATE | O_RDWR);
    if(_file > 0)
    {
        write(_file,ans, strlen(ans));
        close(_file);
    }
    else
    {
        printf(1,"File Dosent Exist");
        exit();
    }
    exit();    
}
